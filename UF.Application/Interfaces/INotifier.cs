﻿namespace UF.Application.Interfaces
{
    public interface INotifier
    {
        void Notify(string msg);
    }
}