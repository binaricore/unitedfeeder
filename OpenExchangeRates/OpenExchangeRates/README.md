**Binance.**

(A .net core worker project.)

**Mission:**

Streaming to redis the ticks (best bid, best ask, time) of selected crypto symbols available by Binance

The web socket stream being used is **@&quot;wss://stream.binance.com:9443/ws/!bookTicker&quot;**

**A. Binance.dll** (in tradingcores/Binance)

1. Listen to Binance provider and Pushes the requested symbols ticks into redis with attached timestamp ( i.e **&quot;Binance.BTCUSDT&quot;**)

2. periodically (5 seconde) sample the members of a redis set (**&quot;binance.symbols&quot;**) and updates the symbols working set (adding &amp; removing)

3. on startup and continuously afterwards

a. Enlist newly encountered binance symbols that are actually providing ticks, and logs out _ **&quot;Available Binance Symbol AUDUSDT&quot;** _

b. Polls binance.symbols and logs out _&quot;__ **Start Listening To Symbol BTCUSDT&quot;, &quot;Stop Listening To Symbol ETCUSDT&quot;** _

**B. Harvester** :

1. Polls periodically (5 seconds) the db&#39;s asset\_feed\_suppliers table

for active or suspended assets that are mapped to binances symbols (&#39;name&#39;= &quot;binance&quot;)

2. Updates &quot;binance.symbols&quot; redis set to the current list of handled symbols.

**Deploy Steps**

_**// Build from solution directory (~/../tradingcore/Binance ):**_

1. &quot;docker build -t gcr.io/proftit-1329/binance:latest .&quot;

_ **// push to gcloud** _

2. &quot;gcloud docker -- push gcr.io/proftit-1329/binance:latest&quot;

_**// From the relevant folder where binance.yml is (tcconfig/int tcconfig/qa manifests/)**_

3. kubectl apply -f binance.yml