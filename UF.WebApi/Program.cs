using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Sinks.Graylog;

namespace UF.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = CreateLoggerConfiguration();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .ConfigureKestrel(x => x.AddServerHeader = false)
                    .UseStartup<Startup>();
                }).UseSerilog();


        private static Serilog.Core.Logger CreateLoggerConfiguration()
        {
            string _grayLogHost = Environment.GetEnvironmentVariable("GRAYLOG_HOST") ?? "graylog";
            var _grayLogPort =
               int.TryParse(Environment.GetEnvironmentVariable("GRAYLOG_PORT"), out int port) ? port : 18514;

            string _grayLogTag = Environment.GetEnvironmentVariable("GRAYLOG_TAG") ?? "unitedfeeder";

            return
                new LoggerConfiguration()
                       .MinimumLevel.Warning()
                       .WriteTo.Console()
                       .WriteTo.Graylog(new GraylogSinkOptions()
                       {
                           HostnameOrAddress = _grayLogHost,
                           Port = _grayLogPort,
                           TransportType = Serilog.Sinks.Graylog.Core.Transport.TransportType.Udp
                       })
                       .Enrich.FromLogContext()
                       .Enrich.WithProperty("tag", _grayLogTag)
                       .CreateLogger();
        }

    }
}
