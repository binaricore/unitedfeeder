﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Helpers;
using UF.Application.Interfaces;
using UF.Application.Queries;
using UF.Core;

namespace UF.Application.Handlers
{
    public class GetFeedDataByNameHandler : IRequestHandler<GetFeedDataByNameRequest, FeedData>
    {
        private IFeedDataRepo _feedDataRepo;

        public GetFeedDataByNameHandler(IFeedDataRepo feedDataRepo)
        {
            _feedDataRepo = feedDataRepo;
        }

        public async Task<FeedData> Handle(GetFeedDataByNameRequest request, CancellationToken cancellationToken)
        {
            return await _feedDataRepo.GetFeedData(request.Name);
        }
    }
}
