﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UF.Application.Helpers;

namespace UF.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplicationLayer(this IServiceCollection services)
        {

            services.AddTransient<CurrencyConvertor>();
            services.AddMediatR(Assembly.GetExecutingAssembly());
            

            return services;
        }
    }
}
