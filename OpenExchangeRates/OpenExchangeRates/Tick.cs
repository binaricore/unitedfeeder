﻿using System;

namespace OpenExchangeRates
{
    internal class Tick
    {
        public decimal Buy  { get; set; } 
        public decimal Sell { get; set; } 
        public decimal Last { get; set; }
        public DateTimeOffset Time { get; set; }
    }
}