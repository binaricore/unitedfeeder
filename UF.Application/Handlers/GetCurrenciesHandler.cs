﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Application.Queries;

namespace UF.Application.Handlers
{
    public class GetCurrenciesHandler : IRequestHandler<GetCurrenciesRequest, IEnumerable<string>>
    {
        private IFeedDataRepo _feedDataRepo;

        public GetCurrenciesHandler(IFeedDataRepo feedDataRepo)
        {
            _feedDataRepo = feedDataRepo;
        }

        public async Task<IEnumerable<string>> Handle(GetCurrenciesRequest request, CancellationToken cancellationToken)
        {
            return await _feedDataRepo.GetCurrenciesListAsync();
        }
    }
}
