﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace UF.WebApi.Models.Requests
{
    public class HistoricalRequest
    {
        
        [FromQuery(Name = "base")]
        public string @Base { get; set; } = "USD";

        [FromQuery(Name = "symbols")]
        public IEnumerable<string> Symbols { get; set; }

    }
}
