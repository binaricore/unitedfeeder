﻿using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Core;


namespace UF.Infra
{
    public class FeedDataRepo : IFeedDataRepo
    {
        private const string UNITED_FEEDER_CONFIGURATION = "united.feeder.config";
        internal ConcurrentDictionary<string, FeedData> FeedDataDictionary { get; set; } = new ConcurrentDictionary<string, FeedData>();

        private IConnectionMultiplexer _multiplexer;
        private ISerializer _serializer;
        private ILogger<Repo> _logger;

        public FeedDataRepo(IConnectionMultiplexer multiplexer
                    , ISerializer serializer
                    , ILogger<Repo> logger)
        {
            _multiplexer = multiplexer;
            _serializer = serializer;
            _logger = logger;
        }
       
        public async Task<List<FeedData>> AddOrUpdateFeedData(FeedData feedData)
        {
            if (feedData is null)
            {
                throw new FeedDataNullException();
            }
            var redis = _multiplexer.GetDatabase();
            FeedDataDictionary.TryGetValue(feedData.Name, out FeedData originalfeedData);
            FeedDataDictionary.AddOrUpdate(feedData.Name, feedData, (_,_) => feedData);
            try
            {
                await redis.StringSetAsync(UNITED_FEEDER_CONFIGURATION, _serializer.Serialize(FeedDataDictionary.Values.ToList()));
            }
            catch (Exception ex)
            {
                // rollback
                FeedDataDictionary.AddOrUpdate(originalfeedData.Name, originalfeedData, (_, _) => originalfeedData);
                throw new FeedDataSaveException(ex.Message);
            }
            return await Task.FromResult(result: FeedDataDictionary.Values.ToList());
        }

        public async Task<List<FeedData>> AddOrUpdateFeedDataList(List<FeedData> feedDataList)
        {
            if (feedDataList is null ||
                feedDataList.Count == 0)
            {
                throw new FeedDataNullException();
            }

            var redis = _multiplexer.GetDatabase();

            foreach (var feedData in feedDataList)
            {
                FeedDataDictionary.AddOrUpdate(feedData.Name, feedData, (_, _) => feedData);
            }
            try
            {
                await redis.StringSetAsync(UNITED_FEEDER_CONFIGURATION, _serializer.Serialize(FeedDataDictionary.Values.ToList()));
            }
            catch (Exception ex)
            {
                throw new FeedDataSaveException(ex.Message);
            }
            return await Task.FromResult(result: FeedDataDictionary.Values.ToList());
        }


        public async Task<List<FeedData>> DeleteFeedData(string name)
        {
            var redis = _multiplexer.GetDatabase();
            if (FeedDataDictionary.TryRemove(name, out FeedData feedData))
            { 
                try
                {
                    await redis.StringSetAsync(UNITED_FEEDER_CONFIGURATION, _serializer.Serialize(FeedDataDictionary.Values.ToList()));
                }
                catch (Exception ex)
                {
                    // rollback
                    FeedDataDictionary.AddOrUpdate(feedData.Name, feedData, (_, _) => feedData);
                    throw new FeedDataSaveException(ex.Message);
                } 
            }
            return await Task.FromResult(FeedDataDictionary.Values.ToList());
        }

        public Task<List<FeedData>> GetFeedData(string symbol, string provider)
        {
            var result = FeedDataDictionary
                            .Where(fd => fd.Value.Sources.Any(ps => ps.ProviderName == provider &&
                                                                    ps.SymbolName == symbol))
                            .Select(fd => fd.Value)
                            .ToList();

            return Task.FromResult(result);
        }

        public Task<List<FeedData>> GetFeedData()
        {
            var result = FeedDataDictionary
                            .Select(fd => fd.Value)
                            .ToList();

            return Task.FromResult(result);
        }

        public async Task<FeedData> GetFeedData(string symbol)
        {
            FeedData result = null;
            if (!(FeedDataDictionary.TryGetValue(symbol, out result)))
            {
                throw new FeedDataNotFoundException();
            };

            return await Task.FromResult(result);
        }

        public async Task<List<string>> GetFeedName(string symbol, string provider)
        {
            var feedData = await GetFeedData(symbol, provider);
            return await Task.FromResult(feedData.Select(fd => fd.Name).ToList());
        }

        public async Task<List<FeedInfo>> GetFeedInfoByBaseCurrency(string baseCurrency)
        {
            var result = FeedDataDictionary
                            .Where(fd => fd.Value.BaseCurrency == baseCurrency)
                            .Select(fd => fd.Value.ToFeedInfo())
                            .ToList();
            return await Task.FromResult(result);
        }

        public async Task<List<FeedInfo>> GetFeedInfoByOtherCurrency(string otherCurrency)
        {
            var result = FeedDataDictionary
                             .Where(fd => fd.Value.OtherCurrency == otherCurrency)
                             .Select(fd => fd.Value.ToFeedInfo())
                             .ToList();
            return await Task.FromResult(result);
        }

        public async Task<List<FeedInfo>> GetFeedInfoByCurrenciesPair(string firstCurrency, string secondCurrency)
        {
            var result = FeedDataDictionary
                             .Where(fd =>
                                    (fd.Value.BaseCurrency == firstCurrency && fd.Value.OtherCurrency == secondCurrency)
                                          ||
                                    (fd.Value.BaseCurrency == secondCurrency && fd.Value.OtherCurrency == firstCurrency))
                             .Select(fd => fd.Value.ToFeedInfo())
                             .ToList();
            return await Task.FromResult(result);
        }

        public async Task<List<FeedInfo>> GetFeedInfo(string baseCurrency, string otherCurrency)
        {
            var result = FeedDataDictionary
                             .Where(fd => fd.Value.BaseCurrency == baseCurrency &&
                                          fd.Value.OtherCurrency == otherCurrency)
                             .Select(fd => fd.Value.ToFeedInfo())
                             .ToList();

            return await Task.FromResult(result);
        }

        public async Task<bool> IsExistFeed(string baseCurrency, string otherCurrency)
            =>
            (await GetFeedInfo(baseCurrency, otherCurrency))
            .Any();

        public async Task<List<string>> GetCurrenciesListAsync()
        {
            {
                List<string> currencies = 
                    FeedDataDictionary.
                       Select(fd => fd.Value.BaseCurrency)
                        .Union(
                    FeedDataDictionary.
                       Select(fd => fd.Value.OtherCurrency))
                       .Distinct()
                       .Where(curr => curr != null)
                       .ToList();

                return await Task.FromResult(currencies);
            }
        }

       
    }
}
