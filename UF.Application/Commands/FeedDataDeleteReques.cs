﻿using MediatR;
using System.Collections.Generic;
using UF.Core;

namespace UF.Application.Commands
{
    public class FeedDataDeleteRequest : IRequest<List<FeedData>>
    {
        public string Name { get; set; }

        public FeedDataDeleteRequest(string name) => Name = name;
    }
}