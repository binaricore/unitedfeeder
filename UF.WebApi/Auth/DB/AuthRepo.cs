﻿using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.Application.Interfaces;

namespace UF.WebApi.Auth.DB
{
    public class AuthRepo
    {
        private IDatabase _redis;
        private ISerializer _serializer;
        private ILogger<AuthRepo> _logger;

        public AuthRepo(IConnectionMultiplexer multiplexer
                        , ISerializer serializer
                        , ILogger<AuthRepo> logger)
        {
            _redis = multiplexer.GetDatabase();
            _serializer = serializer;
            _logger = logger;
        }

        public async Task<string> GetTokenAsync(string token)
        {
            string key = "token." + token;
            var redisValue = await _redis.StringGetAsync(key);
            return (redisValue.ToString());
        }

        public async Task<bool> SetTokenAsync(string token, string  authDetails)
        {
            string key = "token." + token;
            var result = await _redis.StringSetAsync(key, authDetails);
            return result;
        }

        public async Task<bool> DeleteTokenAsync(string token)
        {
            string key = "token." + token;
            var result = await _redis.KeyDeleteAsync(key);
            return result;
        }


    }
}
