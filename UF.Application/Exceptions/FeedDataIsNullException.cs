﻿using System;
using System.Runtime.Serialization;

namespace UF.Infra
{
    [Serializable]
    public class FeedDataNullException : Exception
    {
        public FeedDataNullException()
        {
            
        }

        public FeedDataNullException(string message) : base(message)
        {
        }

        public FeedDataNullException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FeedDataNullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}