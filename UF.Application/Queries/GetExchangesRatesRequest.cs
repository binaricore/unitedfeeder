﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UF.Application.Queries
{
    public class GetExchangesRatesRequest : IRequest<Dictionary<string, decimal>>
    {
        
        public string BaseCurrency { get; set; }
        public IEnumerable<string> Currencies { get; set; }
        public DateTime Date { get; set; } = DateTime.UtcNow;
        public GetExchangesRatesRequest(string baseCurrency,IEnumerable<string> symbols) 
                                           => (BaseCurrency ,Currencies) = (baseCurrency, symbols);
        public GetExchangesRatesRequest(string baseCurrency, IEnumerable<string> symbols, DateTime date)
                                           => (BaseCurrency ,Currencies,Date) = (baseCurrency, symbols,date);
        
    }
}
