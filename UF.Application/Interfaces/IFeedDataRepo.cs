﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Core;

namespace UF.Application.Interfaces
{
    public interface IFeedDataRepo
    {
        public Task<List<FeedData>> AddOrUpdateFeedData(FeedData feedData);
        public Task<List<FeedData>> AddOrUpdateFeedDataList(List<FeedData> feedDataList);
        public Task<List<FeedData>> GetFeedData();
        public Task<List<FeedData>> GetFeedData(string symbol, string provider);
        public Task<List<string>> GetFeedName(string symbol, string provider);
        public Task<List<FeedInfo>> GetFeedInfoByBaseCurrency(string baseCurrency);
        public Task<List<FeedInfo>> GetFeedInfoByOtherCurrency(string otherCurrency);
        public Task<List<FeedInfo>> GetFeedInfoByCurrenciesPair(string firstCurrency, string secondCurrency);
        public Task<List<FeedData>> DeleteFeedData(string name);
        public Task<List<FeedInfo>> GetFeedInfo(string baseCurrency, string otherCurrency);
        public Task<bool> IsExistFeed(string baseCurrency, string otherCurrency);
        public Task<List<string>> GetCurrenciesListAsync();
        public Task<FeedData> GetFeedData(string symbol);
    }
}
