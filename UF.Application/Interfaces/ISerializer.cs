﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UF.Application.Interfaces
{
    public interface ISerializer
    {
        public string Serialize<T>(T obj);

        public T Deserialize<T>(string str);
    }
}
