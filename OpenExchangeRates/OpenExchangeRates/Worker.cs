using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.SymbolStore;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.WebSockets;
using System.Reactive.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;


namespace OpenExchangeRates
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly IDatabase _redis;
        private readonly ISubscriber _redisSubscriber;
        private readonly string _redisPort;
        private readonly string _redisIP;
        private readonly string _openExchangeRatesAPI;
        private string _openExchangeRatesAppId;
        private readonly int _sampleRate;
        //private readonly List<string> _currencies;

        ConcurrentDictionary<string, bool> workSymbols = new ConcurrentDictionary<string, bool>();

        public Worker(ILogger<Worker> logger)
        {
            _redisIP = Environment.GetEnvironmentVariable("REDIS_IP") ?? "localhost";
            _redisPort = Environment.GetEnvironmentVariable("REDIS_PORT") ?? "6379";
            _openExchangeRatesAPI = Environment.GetEnvironmentVariable("OPEN_EXCHANGE_RATES_API") ?? @"https://openexchangerates.org/api/";
            _openExchangeRatesAppId = Environment.GetEnvironmentVariable("OPEN_EXCHANGE_RATES_API_ID") ?? @"8abccdd71d2244209e86b016ae5d7e75";
            _sampleRate = int.TryParse(Environment.GetEnvironmentVariable("SAMPLE_RATE_SECONDS"), out int samplRate) ? samplRate : 600;
            _logger = logger;


            ConnectionMultiplexer redisMultiplexer = ConnectionMultiplexer.ConnectAsync(new ConfigurationOptions()
            {
                EndPoints = { _redisIP + ":" + _redisPort },
                SyncTimeout = int.MaxValue,
                ConnectRetry = 10,
                ConnectTimeout = 30000
            }, Console.Out).Result;
            _redis = redisMultiplexer.GetDatabase();
            _redisSubscriber = redisMultiplexer.GetSubscriber();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(() =>
            {
                HarvestOpenExchangeRatesPeriodically(stoppingToken);
                SubscribeToRatesRequests(stoppingToken);
            });
        }


        private void HarvestOpenExchangeRatesPeriodically(CancellationToken stoppingToken)
        {
            Observable.Timer(DateTimeOffset.UtcNow, TimeSpan.FromSeconds(_sampleRate))
                                             .Subscribe(
                                             _ => GetOpenExchangeRatesLatest(),
                                             (ex) => _logger.LogError($"exception : {ex}"),
                                             () => _logger.LogError("completed")
                                             , stoppingToken);
        }

        private void SubscribeToRatesRequests(CancellationToken stoppingToken)
        {
            try
            {
                _logger.LogInformation("Subscribing to rates requests: openexchange.rates.request.*");
                _redisSubscriber.Subscribe("openexchange.rates.request.*")
                                .OnMessage(HandleRatesRequest());
            }
            catch (Exception ex )
            {
                _logger.LogError("SubscribeToRatesRequests failed", ex);
                throw;
            }
        }

        private Action<ChannelMessage> HandleRatesRequest()
        {
            try
            {
                return async msg =>
                   {
                       try
                       {
                           var request = JsonSerializer.Deserialize<RatesRequest>(msg.Message);
                           var date = request.Time.DateTime;
                           
                           _logger.LogInformation($"HandleRatesRequest date: {date}");
                           
                           var openExchangeRates = await GetOpenExchangeRates(date);
                           if (openExchangeRates == null)
                           {
                               await Task.Delay(2000);
                               _logger.LogInformation($"HandleRatesRequest retrying (after 2 sec), GetOpenExchangeRates, msg:{msg.Message}, date: {date}");
                               openExchangeRates = await GetOpenExchangeRates(date);
                           }
                           await PublishOpenExchangeRates(date, openExchangeRates, request.Id);
                       }
                       catch (Exception ex)
                       {
                           _logger.LogError($"HandleRatesRequest error, msg:{msg.Message}, {ex.Message}");
                       }
                   };
            }
            catch (Exception ex)
            {
                _logger.LogError($"HandleRatesRequest error", ex.Message);
                throw;               
            }
        }

        private async void GetOpenExchangeRatesLatest()
        {
            try
            {
                _logger.LogInformation($"GetOpenExchangeRatesLatest");                
                var webClient = new WebClient();
                var res = await
                    webClient.DownloadStringTaskAsync
                    (@$"https://openexchangerates.org/api/latest.json?app_id={_openExchangeRatesAppId}&base=USD");

                var openExchangeRates = JsonSerializer.Deserialize<OpenExchangeRates>(res);
                await PublishOpenExchangeRates(openExchangeRates);
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetOpenExchangeRatesLatest: {ex.Message}");
            }
        }

        private async Task<bool> PublishOpenExchangeRates(OpenExchangeRates openExchangeRates)
        {
            foreach (var rate in openExchangeRates.rates)
            {
                var baseCurrency = openExchangeRates.@base;
                var otherCurrency = rate.Key;
                var channel = "openexchange." +
                               baseCurrency + 
                               otherCurrency;
                                
                    
                var tick = new Tick()
                {
                    Buy = rate.Value,
                    Sell = rate.Value,
                    Last = 0, 
                    Time = DateTimeOffset.FromUnixTimeSeconds(openExchangeRates.timestamp)
                };
                await _redis.PublishAsync(channel, JsonSerializer.Serialize(tick));
            }
            return true;
        }

        private async Task<bool> PublishOpenExchangeRates(DateTime date, OpenExchangeRates openExchangeRates, string requestId)
        {
            Dictionary<string, Tick> response = new Dictionary<string, Tick>();
            foreach (var rate in openExchangeRates.rates)
            {
                var symbol = openExchangeRates.@base + rate.Key; 
                var channel = "openexchange."
                    + symbol + "."
                    + date.ToString("yyyy-MM-dd");
                var tick = new Tick()
                {
                    Buy = rate.Value,
                    Sell = rate.Value,
                    Last = 0,
                    Time = DateTimeOffset.FromUnixTimeSeconds(openExchangeRates.timestamp)
                };
                await _redis.PublishAsync(channel, JsonSerializer.Serialize(tick));
                response.Add(symbol, tick);
            }
            await _redis.PublishAsync($@"openexchange.rates.response.{requestId}", JsonSerializer.Serialize(response));
            return true;
        }

        private async Task<OpenExchangeRates> GetOpenExchangeRates(DateTime requestDateTime)
        {
            try
            {
                var webClient = new WebClient();
                var day = requestDateTime.Date.ToString("yyyy-MM-dd");
                var res = await
                     webClient.DownloadStringTaskAsync
                     (@$"https://openexchangerates.org/api/historical/{day}.json?app_id={_openExchangeRatesAppId}&base=USD");

                var openExchangeRates = JsonSerializer.Deserialize<OpenExchangeRates>(res);
                return openExchangeRates;
            }
            catch (Exception ex)
            {
                _logger.LogError($"GetOpenExchangeRates {requestDateTime.Date.ToString("yyyy-MM-dd")}  {ex.Message}");
                return null;
            }
        }
    }

    internal class RatesRequest
    {
        public string Id { get; set; }
        public DateTimeOffset Time { get; set; }
    }
}
