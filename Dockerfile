#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0.8-buster-slim AS base
WORKDIR /app
COPY ["united.feeder.config.txt", ""]

FROM mcr.microsoft.com/dotnet/sdk:5.0.302-buster-slim AS build
WORKDIR /src
COPY ["UF.WebApi/UF.WebApi.csproj", "UF.WebApi/" ]
COPY ["UF.Infra/UF.Infra.csproj", "UF.Infra/"]
COPY ["UF.Application/UF.Application.csproj", "UF.Application/"]
COPY ["UF.Core/UF.Core.csproj", "UF.Core/"]
COPY ["UF.FeedWorker/UF.FeedWorker.csproj", "UF.FeedWorker/"]
COPY ["UF.Tests/UF.Tests.csproj", "UF.Tests/"]
COPY ["UF.Monitor/UF.Monitor.csproj", "UF.Monitor/"]
COPY ["UnitedFeeder.sln", ""]
RUN dotnet restore "./UnitedFeeder.sln"
COPY . .
WORKDIR "/src/."
RUN dotnet build  -c Release -o /app/build

FROM build AS publish
RUN dotnet publish  -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "UF.WebApi.dll"]