﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Application.Queries;
using UF.WebApi.Auth;
using UF.WebApi.Models.Requests;

namespace UF.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UsersRoles.MultipleRolesForAuthorize.ADMIN_OR_REGULAR)]
    public class HistoricalController : ControllerBase
    {
        private ILogger<HistoricalController> _logger;
        private ISender _mediator;
        private INotifier _notifier;

        public HistoricalController(ILogger<HistoricalController> logger,
                                    ISender  mediator,
                                    INotifier notifier)
        {
            _logger = logger;
            _mediator = mediator;
            _notifier = notifier;
        }


        [HttpGet]
        [Route("{date}.json")]
        public async Task<IActionResult> GetHisotricalRates([FromRoute] DateTime date,
                                                            [FromQuery] HistoricalRequest parameters)
        {
            var rates = await _mediator.Send(new GetExchangesRatesRequest(parameters.Base,parameters.Symbols,date));

            if (rates == null ||
                rates.Count == 0)
            {
                var notification = $"United Feeder error - date: {date.ToShortDateString()}, base: {parameters.Base}, symbols: {String.Join(", ", parameters.Symbols.ToArray())}.";
                _notifier.Notify(notification);
                _logger.LogError(notification);
            }

            var response = WebApi.Models.Response.CreateWithCurrentTimestamp(parameters.Base, rates);
            return Ok(response);
        }

        [HttpGet]
        [Route("/api/latest.json")]
        public async Task<IActionResult> GetLastestExchangesRatesAsync([FromQuery] HistoricalRequest parameters)
        {
            var rates = await _mediator.Send(new GetExchangesRatesRequest(parameters.Base, parameters.Symbols, DateTime.UtcNow));

            var response = WebApi.Models.Response.CreateWithCurrentTimestamp(parameters.Base, rates);
            return Ok(response);
        }
    }
}
