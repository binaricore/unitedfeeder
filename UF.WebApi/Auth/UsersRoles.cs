﻿namespace UF.WebApi.Auth
{
    public class UsersRoles
    {
        public const string REGULAR_USER = "REGULAR_USER";
        public const string ADMIN_USER = "ADMIN_USER";

        public class MultipleRolesForAuthorize
        {
            public const string ADMIN_OR_REGULAR = "ADMIN_USER,REGULAR_USER";
        }

    }
}
