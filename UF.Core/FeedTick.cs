﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UF.Core
{
    public class FeedTick
    {
        public string Provider { get; set; }
        public string Symbol { get; set; }
        public decimal Bid { get; set; }
        public decimal Ask { get; set; }
        public decimal Close { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
