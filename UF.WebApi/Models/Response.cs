﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace UF.WebApi.Models
{
    public class Response
    {
        public string Disclaimer { get; set; } = "";
        
        public string License { get; set; } = "";
        
        public long Timestamp { get; set; } 
        
        [JsonPropertyName("base")]
        public string BaseSymbol { get; set; }
        
        public Dictionary<string,decimal> Rates { get; set; }

        public static Response Create(DateTimeOffset time,string baseSymbol, Dictionary<string,decimal> rates)
        {
            var response = new Response();
            response.Timestamp = time.ToUnixTimeSeconds();
            response.BaseSymbol = baseSymbol;
            response.Rates = rates;
            return response;
        }
        public static Response CreateWithCurrentTimestamp(string baseSymbol, Dictionary<string,decimal> rates)
        {
            var response = new Response();
            response.Timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
            response.BaseSymbol = baseSymbol;
            response.Rates = rates;
            return response;
        }
    }
}
