﻿using System;
using System.Runtime.Serialization;

namespace UF.Infra
{
    [Serializable]
    public class FeedDataNotFoundException : Exception
    {
        public FeedDataNotFoundException()
        {
        }

        public FeedDataNotFoundException(string message) : base(message)
        {
        }

        public FeedDataNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FeedDataNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}