using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.Graylog;

namespace OpenExchangeRates
{
    public class Program
    {

        public static void Main(string[] args)
        {  
            Log.Logger = CreateLoggerConfiguration();
            CreateHostBuilder(args).Build().Run();
        }

        private static Serilog.Core.Logger CreateLoggerConfiguration()
        {
            string _grayLogHost = Environment.GetEnvironmentVariable("GRAYLOG_HOST") ?? "graylog";
            var    _grayLogPort =
               int.TryParse(Environment.GetEnvironmentVariable("GRAYLOG_PORT"), out int port) ? port : 18514;

            string _grayLogTag = Environment.GetEnvironmentVariable("GRAYLOG_TAG") ?? "tradingcore";

            return 
                new LoggerConfiguration()
                       .WriteTo.Console() 
                       .WriteTo.Graylog(new GraylogSinkOptions()
                       {
                           HostnameOrAddress = _grayLogHost,
                           Port = _grayLogPort,
                           TransportType = Serilog.Sinks.Graylog.Core.Transport.TransportType.Udp
                       })
                       .Enrich.FromLogContext()
                       .Enrich.WithProperty("tag", _grayLogTag)
                       .CreateLogger();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                });
    }
}
