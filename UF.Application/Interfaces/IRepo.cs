﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Core;

namespace UF.Application.Interfaces
{
    public interface IRepo
    {
        Task<bool> TickExistsAsync(string feedName, DateTime date);
        Task<FeedTick> GetTickAsync(string feedName,DateTime date);
        Task<IEnumerable<FeedTick>> GetTicksAsync(IEnumerable<string> requestedFeedNames,DateTime date);
        Task<IEnumerable<string>> GetCurrenciesListAsync();
        Task<bool> RequestRatesForDate(DateTime date);

    }
}
