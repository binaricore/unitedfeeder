﻿using MediatR;
using System.Collections.Generic;
using UF.Core;

namespace UF.Application.Commands
{
    public class FeedDataAddRequest : IRequest<List<FeedData>>
    {
        public List<FeedData> FeedData { get; set; }

        public FeedDataAddRequest(List<FeedData> feedData) => FeedData = feedData;
    }
   
}