﻿using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Helpers;
using UF.Application.Interfaces;
using UF.Application.Queries;

namespace UF.Application.Handlers
{
    public class GetExchangesRatesHandler : IRequestHandler<GetExchangesRatesRequest, Dictionary<string, decimal>>
    {
        private IRepo _repo;
        private IFeedDataRepo _feedDataRepo;
        private CurrencyConvertor _convertor;
        private ILogger<GetExchangesRatesHandler> _logger;

        public GetExchangesRatesHandler(IRepo repo,
                                        IFeedDataRepo feedDataRepo,
                                        CurrencyConvertor convertor,
                                        ILogger<GetExchangesRatesHandler> logger)
        {
            _repo = repo;
            _feedDataRepo = feedDataRepo;
            _convertor = convertor;
            _logger = logger;
        }


        public async Task<Dictionary<string, decimal>> Handle(GetExchangesRatesRequest request, CancellationToken cancellationToken)
        {
            var resultDic = new Dictionary<string, decimal>();
            IEnumerable<string> targetCurrencies = await GetCurrencies(request);

            if (await FeedsExistForDate(request.Date) == false)
            {
                _logger.LogInformation($"BringFeedsForDate for date: {request.Date}");
                if (!(await BringFeedsForDate(request.Date)))
                {
                    _logger.LogError($"BringFeedsForDate timeout: {request.Date}");
                    return resultDic;
                }
                
                // Wait for feed to be generated
                var cts = new CancellationTokenSource();
                cts.CancelAfter(10000);
                bool shouldBreak = false;
                while (!shouldBreak)
                {
                    await Task.Delay(25);
                    if (cts.IsCancellationRequested)
                    {
                        break;
                    }
                    shouldBreak = await FeedsExistForDate(request.Date);
                    if (shouldBreak)
                    {
                        await Task.Delay(1000);
                    }
                }
            }

            var exchangeRatesRequests = new Dictionary<string, Task<decimal>>();
            foreach (var targetCurrency in targetCurrencies)
            {
                if (targetCurrency != null)
                {
                    exchangeRatesRequests.Add(targetCurrency, _convertor.Convert(amount: 1,
                                                                   sourceCurrency: request.BaseCurrency.ToUpper(),
                                                                   targetCurrency: targetCurrency.ToUpper(),
                                                                   date: request.Date)
                                                                   );
                }
                else 
                {
                    _logger.LogError($"why target: {targetCurrency} is null");
                }
            }

            await Task.WhenAll(exchangeRatesRequests.Values);
            
            foreach (var task in exchangeRatesRequests)
            {
                if (task.Value.Result != 0)
                {
                    resultDic.Add(task.Key, task.Value.Result);
                }
            }

            return resultDic;
        }

        private async Task<bool> BringFeedsForDate(DateTime date)
        {
            return await _repo.RequestRatesForDate(date);
        }

        private Task<bool> FeedsExistForDate(DateTime date)
        {
            // We are searching if some feed from this day exists.
            // So USDGBP is something very regular, so it will be our test.
            return _repo.TickExistsAsync("USDGBP", date);
        }

        private async Task<IEnumerable<string>> GetCurrencies(GetExchangesRatesRequest request)
        {
            IEnumerable<string> currencies;

            if (request.Currencies != null)
            {
                currencies = request.Currencies;
            }
            else
            {
                currencies = await _feedDataRepo.GetCurrenciesListAsync();
            }

            return currencies;
        }
    }
}
