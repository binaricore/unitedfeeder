﻿using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Core;

namespace UF.Infra
{
    public class Repo : IRepo
    {
        private IDatabase _redis;
        private ISerializer _serializer;
        private ILogger<Repo> _logger;
        private IConnectionMultiplexer _redisConnectionMultiplexer;

        public Repo(IConnectionMultiplexer multiplexer
                    ,ISerializer serializer
                    ,ILogger<Repo> logger)
        {
            _redisConnectionMultiplexer = multiplexer;
            _redis = multiplexer.GetDatabase();
            _serializer = serializer;
            _logger = logger;
        }

        private string KeyOf(string symbol, DateTime date) => $"Feed.{symbol}.{date.ToString("yyyy-MM-dd")}";
        
        private const string AVAILABLE_SYMBOLS_KEY_NAME = "available_symbols";
       
        public async Task<FeedTick> GetTickAsync(string feedName, DateTime date)
        {
            FeedTick tick = null;

            var feedRedisKey = KeyOf(feedName, date);
            var v = await _redis.StringGetAsync(feedRedisKey);
            if (!v.IsNullOrEmpty)
                tick =   _serializer.Deserialize<FeedTick>(v.ToString());

            return tick;
        }
        public async Task<bool> TickExistsAsync(string feedName, DateTime date)
        {
            var feedRedisKey = KeyOf(feedName, date);
            return await _redis.KeyExistsAsync(feedRedisKey);
        }
        public async Task<IEnumerable<FeedTick>> GetTicksAsync(IEnumerable<string> requestedFeedNames,DateTime date)
        {
            var list = new List<FeedTick>();
            foreach (var feedName in requestedFeedNames)
            {
                var feedRedisKey = KeyOf(feedName, date);
                var redisValue = await _redis.StringGetAsync(feedRedisKey);
                if (!redisValue.IsNullOrEmpty)
                {
                    var tick = _serializer.Deserialize<FeedTick>(redisValue) ;
                    list.Add(tick);
                }
                else
                {
                    _logger.LogWarning($"feed {feedName} not exists in redis.");  
                }
            }
            return list;
        }

       

        public async Task<IEnumerable<string>> GetCurrenciesListAsync()
        {
           
            var redisSet = await _redis
                             .SetMembersAsync(AVAILABLE_SYMBOLS_KEY_NAME);

            var symbols = redisSet
                            .Select(x=>x.ToString())
                            .ToList();
            return symbols;
        }

        public async Task<bool> RequestRatesForDate(DateTime date)
        {
            var subscriber = _redisConnectionMultiplexer.GetSubscriber();
            var request = RatesRequest.Create(date);
            string requestKey = $"openexchange.rates.request.{request.Id}";
            string responseKey = $"openexchange.rates.response.{request.Id}";
            var cts = new CancellationTokenSource();
            cts.CancelAfter(10000);

            var responseArrived = false;
            await subscriber.SubscribeAsync(responseKey, (channel, value) => {
                responseArrived = true;
            });

            _logger.LogInformation($"RequestRatesForDate : requesting rates for {date}, on {requestKey}");
            await _redis.PublishAsync(requestKey,_serializer.Serialize(request));

            while(!responseArrived)
            {
                await Task.Delay(25);
                if (cts.IsCancellationRequested)
                {
                    break;
                }
            }

            _logger.LogInformation($"RequestRatesForDate : responseArrived {responseArrived}");
            return responseArrived;
        }

        private class RatesRequest
        {
            public string Id { get; set; }
            public DateTimeOffset Time { get; set; }
            public static RatesRequest Create(DateTime date)
            {
                var r = new RatesRequest
                {
                    Id = Guid.NewGuid().ToString("N"),
                    Time = new DateTimeOffset(date)
                };
                return r;
            }
        }
    }
}
