﻿using System;
using System.Runtime.Serialization;

namespace UF.Infra
{
    [Serializable]
    public class FeedDataSaveException : Exception
    {
        public FeedDataSaveException()
        {
        }

        public FeedDataSaveException(string message) : base(message)
        {
        }

        public FeedDataSaveException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FeedDataSaveException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}