﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Primitives;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using UF.WebApi.Auth.DB;

namespace UF.WebApi.Auth
{
    public class AuthHandler : AuthenticationHandler<AuthOptions> 
    {
        #region locals
        private const string AUTH_KEY_NAME = "app_id";
        public const string AUTHENTICATION_SCHEME_NAME = "MAIN";

        private AuthRepo _authRepo;
        private ILogger<AuthHandler> _logger;
        #endregion

        #region constructor
        public AuthHandler(
            // ------ Requested by base class: 'AuthenticationHandler'.
            IOptionsMonitor<AuthOptions> options,
            ILoggerFactory logger, 
            UrlEncoder encoder, 
            ISystemClock clock,
            // ------ Needed to our class
            AuthRepo authRepo
            ) : base(options,logger,encoder,clock)
        {
            _authRepo = authRepo;
            _logger = logger.CreateLogger<AuthHandler>();        }
        #endregion

        protected async override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            try
            {
                if (!Context.Request.Query.ContainsKey(AUTH_KEY_NAME))
                    return AuthenticateResult.Fail(AuthMessages.MISSING_TOKEN);

                Context.Request.Query.TryGetValue(AUTH_KEY_NAME, out StringValues appId);
                var tokenProperties = await _authRepo.GetTokenAsync(appId.ToString());

                if (!string.IsNullOrEmpty(tokenProperties))
                {

                    var authToken = System.Text.Json.JsonSerializer.Deserialize<AuthDetails>(tokenProperties);
                    if (string.IsNullOrWhiteSpace(authToken.UserRole))
                        return AuthenticateResult.Fail(AuthMessages.TOKEN_BODY_CANT_DESERIALIZED);

                    var identity = new ClaimsIdentity(claims            : null,
                                                      authenticationType: AUTHENTICATION_SCHEME_NAME);
                    identity.AddClaim(new Claim(ClaimTypes.Role, authToken.UserRole));


                    var principal = new ClaimsPrincipal(identity);

                    _logger.LogInformation($"User: {authToken.Name} authenticated successfully. (Role: {authToken.UserRole}).");


                    return AuthenticateResult.Success(new AuthenticationTicket(principal :           principal,
                                                                               properties:           null,
                                                                               authenticationScheme: AUTHENTICATION_SCHEME_NAME));
                }

                return AuthenticateResult.Fail(AuthMessages.TOKEN_NOT_EXISTS);
            }
            catch (Exception e)
            {
                _logger.LogError(e, AuthMessages.ERROR_IN_AUTH);
                return AuthenticateResult.Fail(AuthMessages.ERROR_IN_AUTH);
            }
        }

        class AuthMessages
        {
            public const string MISSING_TOKEN    = "Missing app_id token inside the query string parameters.";
            public const string TOKEN_NOT_EXISTS = "this token not exists in our records!";
            public const string TOKEN_BODY_CANT_DESERIALIZED = "token body cant deserialized ";
            public const string ERROR_IN_AUTH = "error in auth";
        }
    }
}
