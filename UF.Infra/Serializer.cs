﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Application.Interfaces;

namespace UF.Infra
{
    public class Serializer : ISerializer
    {
        public T Deserialize<T>(string str)
        {
            return System.Text.Json.JsonSerializer.Deserialize<T>(str,new System.Text.Json.JsonSerializerOptions { PropertyNameCaseInsensitive = true });
        }

        public string Serialize<T>(T obj)
        {
            return System.Text.Json.JsonSerializer.Serialize(obj);
        }
    }
}
