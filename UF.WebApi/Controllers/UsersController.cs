﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.WebApi.Auth;
using UF.WebApi.Auth.DB;

namespace UF.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = UsersRoles.ADMIN_USER)]
    public class UsersController : ControllerBase
    {
        private ILogger<UsersController> _logger;
        private AuthRepo _authRepo;

        public UsersController(ILogger<UsersController> logger, AuthRepo authRepo)
        {
            _logger = logger;
            _authRepo = authRepo;
        }

        [HttpGet("{token}")]
        public async Task<IActionResult> Get([FromRoute] string token)
        {
            var body = await _authRepo.GetTokenAsync(token);
            var authToken = System.Text.Json.JsonSerializer.Deserialize<AuthDetails>(body);
            return Ok(authToken);
        }

        [HttpPost("{token}")]
        public async Task<IActionResult> Post([FromRoute] string token,
                                              [FromBody]  AuthDetails authToken)
        {
            var result = await _authRepo.SetTokenAsync(token, System.Text.Json.JsonSerializer.Serialize(authToken));
            return Ok(new { result });
        }

        [HttpDelete("{token}")]
        public async Task<IActionResult> Delete([FromRoute] string token)
        {
            var result = await _authRepo.DeleteTokenAsync(token);
            return Ok(new { result });
        }
    }
}
