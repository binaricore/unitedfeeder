﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Core;

namespace UF.Application.Queries
{
    public class GetFeedDataByNameRequest : IRequest<FeedData>
    {
        public string Name { get; set; } = null;
        public GetFeedDataByNameRequest(string name) => Name = name;
    }
}

