using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Core;
using UF.FeedWorker.Models;

namespace UF.FeedWorker
{
    public class FeedWorker : BackgroundService
    {
        private readonly ILogger<FeedWorker> _logger;
        private readonly ISerializer _serializer;
        private readonly IDatabase _redis;
        private readonly ISubscriber _subscriber;
        private readonly IFeedDataRepo _feedDataRepo;
        bool _configFromFile;
        string _configFileFolderPath; // expected file name 'united.feeder.config.txt'

        public FeedWorker(ILogger<FeedWorker> logger, IConnectionMultiplexer multiplexer, ISerializer serializer, IFeedDataRepo feedMapper)
        {
            _logger = logger;
            _serializer = serializer;
            _redis = multiplexer.GetDatabase();
            _subscriber = multiplexer.GetSubscriber();
            _feedDataRepo = feedMapper;
            _configFromFile = bool.TryParse(Environment.GetEnvironmentVariable("CONFIG_FROM_FILE"), out _configFromFile) ? _configFromFile : false;
            _configFileFolderPath = Environment.GetEnvironmentVariable("CONFIG_FILE_FOLDER_PATH") ?? "/app";
            
            LoadFeedConfiguration().Wait();
        }

        private async Task LoadFeedConfiguration()
        {
            try
            {
                var feedDataConfig = await _redis.StringGetAsync("united.feeder.config");
                try
                {
                    if (_configFromFile)
                        feedDataConfig = await File.ReadAllTextAsync($"{_configFileFolderPath}/united.feeder.config.txt");
                }
                catch (Exception ex)
                {
                    _logger.LogError("Could not read feed data from file", ex);
                }

                try
                {
                    await File.WriteAllTextAsync($"../united.feeder.config_{DateTime.UtcNow.ToString("yyyy-MM-dd")}.txt", feedDataConfig);
                }
                catch (Exception ex)
                {
                    _logger.LogError("Could not write feed data to file", ex);
                }

                try
                {
                    var feedData = _serializer.Deserialize<List<FeedData>>(feedDataConfig);
                    foreach (var feedDataItem in feedData)
                    {
                        await _feedDataRepo.AddOrUpdateFeedData(feedDataItem);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError("Feed data backup is corrupted", ex);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("LoadFeedConfiguration Error", ex);
            }
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var providers = new List<string>() { "leverate", "binance", "openexchange" };
            foreach (var provider in providers)
            {
                _logger.LogInformation($"Subscribing to {provider}");
                _subscriber.Subscribe($"{provider}.*").OnMessage(
                async msg =>
                {
                    try
                    {
                        await GenerateFeed(msg.Message, msg.Channel);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError("GenerateFeed error", ex);
                    }
                });
            }

            return Task.CompletedTask;
        }

        private async Task GenerateFeed(RedisValue message, RedisChannel messageChannel)
        {
            try
            {
                var channelDecomposed = messageChannel.ToString()?.Split(".");
                if (channelDecomposed.Any())
                {
                    var providerTick = _serializer.Deserialize<ProviderTick>(message);

                    var provider = channelDecomposed?[0];
                    var providerSymbol = channelDecomposed?[1];
                    var feedTick = providerTick.ToFeedTick(provider, providerSymbol);

                    var targetName = (await _feedDataRepo.GetFeedName(providerSymbol, provider)).SingleOrDefault();
                    if (targetName is not null)
                    {
                        var date = providerTick.Time.DateTime.ToString("yyyy-MM-dd");
                        var channel = $"Feed.{targetName}.{date}";
                        await _redis.PublishAsync(channel, _serializer.Serialize<FeedTick>(feedTick));
                        await _redis.StringSetAsync(channel, _serializer.Serialize<FeedTick>(feedTick), TimeSpan.FromDays(365 * 5));
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("GenerateFeed error", ex);
                throw;
            }
        }
    }
}
