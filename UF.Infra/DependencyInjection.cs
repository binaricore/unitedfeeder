﻿using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Core;

namespace UF.Infra
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfra(this IServiceCollection services)
        {

            services.AddScoped<IRepo, Repo>();


            var _redisIP = Environment.GetEnvironmentVariable("REDIS_IP") ?? "localhost";
            var _redisPort = Environment.GetEnvironmentVariable("REDIS_PORT") ?? "6379";

            ConnectionMultiplexer redisMultiplexer = ConnectionMultiplexer.ConnectAsync(new ConfigurationOptions()
            {
                EndPoints = { _redisIP + ":" + _redisPort },
                SyncTimeout = int.MaxValue,
                ConnectRetry = 10,
                ConnectTimeout = 30000
            }, Console.Out).Result;

            redisMultiplexer.GetDatabase().StringSet("token.dotnet", "{ \"Name\": \"some user\", \"UserRole\": \"ADMIN_USER\" }");

            services.AddSingleton<IConnectionMultiplexer>(redisMultiplexer);
            services.AddTransient<IDatabase>((_) => redisMultiplexer.GetDatabase());
            services.AddTransient<ISerializer, Serializer>();
            services.AddSingleton<IFeedDataRepo, FeedDataRepo>();
            services.AddSingleton<INotifier, Notifier>();
            return services;
        }



    }
}
