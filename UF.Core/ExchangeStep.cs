﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UF.Core
{
    public class ExchangeStep
    {
        public bool PathFound { get; set; } = false;
        public string FeedName { get; set; }
        public int Order { get; set; }
        public string SourceCurrency { get; set; }
        public string TargetCurrency { get; set; }

    }
}
