﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Core;

namespace UF.FeedWorker.Models
{
    internal class ProviderTick
    {
        public decimal Buy { get; set; }
        public decimal Sell { get; set; }
        public decimal Last { get; set; }
        public DateTimeOffset Time { get; set; }

        internal FeedTick ToFeedTick(string provider, string symbol)
        {
            var result = new FeedTick();
            result.Provider = provider;
            result.Symbol = symbol;
            result.Bid = Buy;
            result.Ask = Sell;
            result.Close = (result.Bid + result.Ask) / 2.0m;
            result.Timestamp = new DateTime(Time.Ticks);
            return result;

        }
    }
}
