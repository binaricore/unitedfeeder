﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using UF.Application.Interfaces;

namespace UF.Infra
{
    public class Notifier : INotifier
    {
        private ILogger<Notifier> _logger;
        bool _shouldNotify;
        string _token;
        string _user;
        string _api;

        public Notifier(ILogger<Notifier> logger)
        {
            _logger = logger;
            bool.TryParse(Environment.GetEnvironmentVariable("NOTIFY"), out _shouldNotify);
            _token = Environment.GetEnvironmentVariable("TOKEN") ?? "a67z6qb8aoxmsb8ioiafjr2jqsewsx";
            _user = Environment.GetEnvironmentVariable("USER") ?? "gnwn4vraocanc7tpuiddm3qu6ny4og";
            _api = Environment.GetEnvironmentVariable("API") ?? "https://api.pushover.net/1/messages.json";
        }

        public void Notify(string msg)
        {
            try
            {
                if (_shouldNotify)
                {
                    HttpClient client = new HttpClient();

                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var stringContent = new FormUrlEncodedContent(new[]
                      {
                    new KeyValuePair<string, string>("token",_token ),
                    new KeyValuePair<string, string>("user", _user),
                    new KeyValuePair<string, string>("message", msg),
             });
                    var response = client.PostAsync(_api, stringContent).Result;


                    if (!response.IsSuccessStatusCode)
                    {
                        _logger.LogError($"Error in notification via pushover");
                    }

                    _logger.LogInformation(msg);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error in notification via pushover");
            }
        }
    }
}
