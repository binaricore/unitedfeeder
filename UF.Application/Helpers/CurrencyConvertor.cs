﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Core;

namespace UF.Application.Helpers
{

    public class CurrencyConvertor
    {
        #region private variables
        private ILogger<CurrencyConvertor> _logger;
        private IFeedDataRepo _metadataRepo;
        private IRepo _repo;
        #endregion

        #region constants
        private const string USD = "USD";
        private const string BTC = "BTC";
        private const string ETH = "ETH";
        private const string USDT = "USDT"; // Tether
        private const string USDC = "USDC";
        private const string BUSD = "BUSD";
        #endregion

        #region constructor
        public CurrencyConvertor(ILogger<CurrencyConvertor> logger,
                                 IFeedDataRepo metadataRepo,
                                 IRepo repo)
        {
            _logger = logger;
            _metadataRepo = metadataRepo;
            _repo = repo;
        }
        #endregion

        #region public methods
        // We are converting to targetCurrencyId (i.e USD)
        // That means that if for example, the asset is EURUSD and it's rate is 1.21.
        // So it means that 1 EUR = 1.21 USD.
        // So to convert 200 EUR for example, we just multiple it : 200 * 1.21 = 242 USD.
        // BUT if the currency is inversed - means the USD is first in the couple.
        // for example USDZAR, so in this case we will divide it.
        public async Task<decimal> Convert(decimal amount, string sourceCurrency, string targetCurrency, DateTime date, bool tryInDirect = true)
        {
            decimal result = 0m;
            try
            {
                if ((amount == 0m) ||
                    (sourceCurrency == targetCurrency))
                {
                    return amount;
                }

                if (IsDirectConvertPossible(sourceCurrency, targetCurrency))
                {
                    try
                    {
                        return await ConvertDirectAsync(amount, sourceCurrency, targetCurrency, date);
                    }
                    catch (Exception)
                    {
                    }
                }

                // Try indirect in case there is no direct exchange between source & target
                if (tryInDirect && (await IsConvertByIndirectStepsIsPossible(sourceCurrency, targetCurrency)))
                {
                    return await ConvertIndirectAsync(amount, await GetIndirectExchangeSteps(sourceCurrency, targetCurrency), date);
                }
                else
                {
                    throw new Exception($"can't convert currency {sourceCurrency} to currency {targetCurrency}");
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"can't convert amount:{amount} base:{sourceCurrency} target:{targetCurrency}", ex);
            }
            return result;
        }
        #endregion

        #region direct convert functions 
        private bool IsDirectConvertPossible(string baseCurrency, string targetCurrency)
        {
            if (_metadataRepo.IsExistFeed(baseCurrency, targetCurrency).Result
                || _metadataRepo.IsExistFeed(targetCurrency, baseCurrency).Result)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private async Task<decimal> ConvertDirectAsync(decimal amount, string baseCurrency, string targetCurrency, DateTime date)
        {
            try
            {
                if ((amount == 0m) ||
                    (baseCurrency == targetCurrency))
                {
                    return amount;
                }

                if (await _metadataRepo.IsExistFeed(baseCurrency, targetCurrency))
                {
                    var feedMetadata = (await _metadataRepo.GetFeedInfo(baseCurrency, targetCurrency)).First();
                    var feedName = feedMetadata.Name;
                    var tick = await _repo.GetTickAsync(feedName, date);
                    if (tick is not null)
                    {
                        return amount * tick.Close;
                    }
                }

                if (await _metadataRepo.IsExistFeed(targetCurrency, baseCurrency))
                {
                    var feedMetadata = (await _metadataRepo.GetFeedInfo(targetCurrency, baseCurrency)).First();
                    var feedName = feedMetadata.Name;
                    var tick = await _repo.GetTickAsync(feedName, date);
                    if (tick is not null &&
                        tick.Close != 0m)
                    {
                        return amount / tick.Close;
                    }
                }
                throw new Exception($"Can't directly convert currency {baseCurrency} to currency {targetCurrency} by rate of date {date.Date}");
            }
            catch (Exception ex)
            {
                _logger.LogError("ConvertDirectAsync " + ex?.InnerException + ex?.Message, ex);
                throw;
            }
        }
        #endregion

        #region indirect convert function 
        private async Task<bool> IsConvertByIndirectStepsIsPossible(string baseCurrency, string targetCurrency)
        {
            var exchangeSteps = await GetIndirectExchangeSteps(baseCurrency, targetCurrency);
            return exchangeSteps.Count() >= 2 && exchangeSteps.All(x => x.PathFound == true);
        }

        private async Task<List<ExchangeStep>> GetIndirectExchangeSteps(string sourceCurrency, string targetCurrency)
        {
            var result = new List<ExchangeStep>();

            try
            {
                var exchangeSteps = new List<ExchangeStep>();

                // 2 steps :  source -> usd + usd -> target .
                exchangeSteps = await GetIndirectExchangeSteps2(sourceCurrency, targetCurrency, USD);
                if (CanConvertIndirect(exchangeSteps, 2))
                    return exchangeSteps;

                // 2 steps :  source -> btc  +   btc -> target.
                exchangeSteps = await GetIndirectExchangeSteps2(sourceCurrency, targetCurrency, BTC);
                if (CanConvertIndirect(exchangeSteps, 2))
                    return exchangeSteps;

                // 2 steps :  source -> eth  +   eth -> target.
                exchangeSteps = await GetIndirectExchangeSteps2(sourceCurrency, targetCurrency, ETH);
                if (CanConvertIndirect(exchangeSteps, 2))
                    return exchangeSteps;

                // 2 steps :  source -> usdt  +   usdt -> target.
                exchangeSteps = await GetIndirectExchangeSteps2(sourceCurrency, targetCurrency, USDT);
                if (CanConvertIndirect(exchangeSteps, 2))
                    return exchangeSteps;

                // 2 steps :  source -> busd  +   busd -> target.
                exchangeSteps = await GetIndirectExchangeSteps2(sourceCurrency, targetCurrency, BUSD);
                if (CanConvertIndirect(exchangeSteps, 2))
                    return exchangeSteps;

                // 2 steps :  source -> usdc  +   usdc -> target.
                exchangeSteps = await GetIndirectExchangeSteps2(sourceCurrency, targetCurrency, USDC);
                if (CanConvertIndirect(exchangeSteps, 2))
                    return exchangeSteps;

                // 3 steps :  source -> btc  +   btc -> usd + usd -> target (i.e. crypto to FIAT)
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, BTC, USD);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> usd   +   usd -> btc + btc -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, USD, BTC);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> eth  +   eth -> usd + usd -> target (i.e. crypto to FIAT)
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, ETH, USD);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> usd   +   usd -> eth + eth -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, USD, ETH);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> usdt   +   usdt -> btc + btc -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, USDT, BTC);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> btc   +   btc -> usdt + usdt -> target (i.e. crypto to FIAT).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, BTC, USDT);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> usdc   +   usdt -> btc + btc -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, USDC, BTC);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> btc   +   btc -> usdt + usdc -> target (i.e. crypto to FIAT).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, BTC, USDC);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> busd   +   busd -> btc + btc -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, BUSD, BTC);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;

                // 3 steps :  source -> btc   +   btc -> busd + busd -> target (i.e. crypto to FIAT).
                exchangeSteps = await GetIndirectExchangeSteps3(sourceCurrency, targetCurrency, BTC, BUSD);
                if (CanConvertIndirect(exchangeSteps, 3))
                    return exchangeSteps;


                // 4 steps :  source -> USDT  +   USDT -> btc +  btc -> usd +  usd -> target (i.e. crypto to FIAT)
                exchangeSteps = await GetIndirectExchangeSteps4(sourceCurrency, targetCurrency, USDT, BTC, USD);
                if (CanConvertIndirect(exchangeSteps, 4))
                    return exchangeSteps;

                // 4 steps :  source -> usd  +   usd -> btc +  btc -> usdt +  usdt -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps4(sourceCurrency, targetCurrency, USD, BTC, USDT);
                if (CanConvertIndirect(exchangeSteps, 4))
                    return exchangeSteps;

                // 4 steps :  source -> usdc  +   usdc -> eth +  eth -> usd +  usd -> target (i.e. crypto to FIAT)
                exchangeSteps = await GetIndirectExchangeSteps4(sourceCurrency, targetCurrency, USDC, ETH, USD);
                if (CanConvertIndirect(exchangeSteps, 4))
                    return exchangeSteps;

                // 4 steps :  source -> usd  +   usd -> eth +  eth -> usdc +  usdc -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps4(sourceCurrency, targetCurrency, USD, ETH, USDC);
                if (CanConvertIndirect(exchangeSteps, 4))
                    return exchangeSteps;

                // 4 steps :  source -> busd  +   busd -> btc +  btc -> usd +  usd -> target (i.e. crypto to FIAT)
                exchangeSteps = await GetIndirectExchangeSteps4(sourceCurrency, targetCurrency, BUSD, BTC, USD);
                if (CanConvertIndirect(exchangeSteps, 4))
                    return exchangeSteps;

                // 4 steps :  source -> usd  +   usd -> btc +  btc -> busd +  busd -> target (i.e. FIAT to crypto).
                exchangeSteps = await GetIndirectExchangeSteps4(sourceCurrency, targetCurrency, USD, BTC, BUSD);
                if (CanConvertIndirect(exchangeSteps, 4))
                    return exchangeSteps;


            }
            catch (Exception ex)
            {
                _logger.LogError("GetIndirectExchangeSteps " + ex?.InnerException + ex?.Message, ex);
                throw;
            }
            return result;
        }

        bool CanConvertIndirect(List<ExchangeStep> exchangeSteps, int count)
        {
            return (exchangeSteps.Count() >= count &&
                    exchangeSteps.All(step => step.PathFound));
        }

        private async Task<List<ExchangeStep>> GetIndirectExchangeSteps2(string sourceCurrency, string targetCurrency, string mediatorCurrency)
        {
            var result = new List<ExchangeStep>();

            // 2 steps :  source -> usd + usd -> target .
            var step1 = await GetExchangeStepToSomeCurrency(sourceCurrency, mediatorCurrency);
            var step2 = await GetExchangeStepToSomeCurrency(mediatorCurrency, targetCurrency);

            if (step1.PathFound
                && step2.PathFound)
            {
                result = CreateConvertionSteps(step1, step2, sourceCurrency, targetCurrency, mediatorCurrency);
            }

            return result;
        }

        private async Task<List<ExchangeStep>> GetIndirectExchangeSteps3(string sourceCurrency, string targetCurrency, string firstMediatorCurrency, string secondMediatorCurrency)
        {
            var result = new List<ExchangeStep>();

            var step1 = await GetExchangeStepToSomeCurrency(sourceCurrency, firstMediatorCurrency);
            var step2 = await GetExchangeStepToSomeCurrency(firstMediatorCurrency, secondMediatorCurrency);
            var step3 = await GetExchangeStepToSomeCurrency(secondMediatorCurrency, targetCurrency);

            if (step1.PathFound
                && step2.PathFound
                && step3.PathFound)
            {
                result = CreateConversionSteps(step1, step2, step3, sourceCurrency, targetCurrency, firstMediatorCurrency, secondMediatorCurrency);
            }

            return result;
        }

        private List<ExchangeStep> CreateConversionSteps(ExchangeStep step1,
                                           ExchangeStep step2,
                                           ExchangeStep step3,
                                           string sourceCurrencyId,
                                           string targetCurrencyId,
                                           string firstMiddleCurrencyId,
                                           string SecondMiddleCurrencyId)
        {
            var result = new List<ExchangeStep>();
            step1.Order = 1;
            step1.SourceCurrency = sourceCurrencyId;
            step1.TargetCurrency = firstMiddleCurrencyId;
            result.Add(step1);

            step2.Order = 2;
            step2.SourceCurrency = firstMiddleCurrencyId;
            step2.TargetCurrency = SecondMiddleCurrencyId;
            result.Add(step2);

            step3.Order = 3;
            step3.SourceCurrency = SecondMiddleCurrencyId;
            step3.TargetCurrency = targetCurrencyId;
            result.Add(step3);
            return result;
        }


        private List<ExchangeStep> CreateConversionSteps(ExchangeStep step1,
                                           ExchangeStep step2,
                                           ExchangeStep step3,
                                           ExchangeStep step4,
                                           string sourceCurrencyId,
                                           string targetCurrencyId,
                                           string firstMiddleCurrencyId,
                                           string SecondMiddleCurrencyId,
                                           string thirdMiddleCurrencyId)
        {
            var result = new List<ExchangeStep>();
            step1.Order = 1;
            step1.SourceCurrency = sourceCurrencyId;
            step1.TargetCurrency = firstMiddleCurrencyId;
            result.Add(step1);

            step2.Order = 2;
            step2.SourceCurrency = firstMiddleCurrencyId;
            step2.TargetCurrency = SecondMiddleCurrencyId;
            result.Add(step2);

            step3.Order = 3;
            step3.SourceCurrency = SecondMiddleCurrencyId;
            step3.TargetCurrency = thirdMiddleCurrencyId;
            result.Add(step3);

            step4.Order = 4;
            step4.SourceCurrency = thirdMiddleCurrencyId;
            step4.TargetCurrency = targetCurrencyId;
            result.Add(step4);
            return result;
        }

        private List<ExchangeStep> CreateConvertionSteps(ExchangeStep step1, ExchangeStep step2, string sourceCurrency, string targetCurrency, string middleCurrency)
        {
            var result = new List<ExchangeStep>();
            step1.Order = 1;
            step1.SourceCurrency = sourceCurrency;
            step1.TargetCurrency = middleCurrency;
            result.Add(step1);

            step2.Order = 2;
            step2.SourceCurrency = middleCurrency;
            step2.TargetCurrency = targetCurrency;
            result.Add(step2);
            return result;
        }

        private async Task<List<ExchangeStep>> GetIndirectExchangeSteps4(string sourceCurrency,
                                                                 string targetCurrency,
                                                                 string firstMediatorCurrency,
                                                                 string secondMediatorCurrency,
                                                                 string thirdMediatorCurrency)
        {
            var result = new List<ExchangeStep>();

            var step1 = await GetExchangeStepToSomeCurrency(sourceCurrency, firstMediatorCurrency);
            var step2 = await GetExchangeStepToSomeCurrency(firstMediatorCurrency, secondMediatorCurrency);
            var step3 = await GetExchangeStepToSomeCurrency(secondMediatorCurrency, thirdMediatorCurrency);
            var step4 = await GetExchangeStepToSomeCurrency(thirdMediatorCurrency, targetCurrency);

            if (step1.PathFound
                && step2.PathFound
                && step3.PathFound
                && step4.PathFound)
            {
                result = CreateConversionSteps(step1, step2, step3, step4, sourceCurrency, targetCurrency, firstMediatorCurrency, secondMediatorCurrency, thirdMediatorCurrency);
            }

            return result;
        }
        private async Task<ExchangeStep> GetExchangeStepToSomeCurrency(string currency, string someCurrency)
        {
            var step = new ExchangeStep();
            if (currency == someCurrency)
            {
                step.PathFound = true;
            }
            else if (await _metadataRepo.IsExistFeed(currency, someCurrency))
            {
                var feedName = _metadataRepo.GetFeedInfo(currency, someCurrency).Result.First().Name;
                step.FeedName = feedName;
                step.PathFound = true;
            }
            else if (await _metadataRepo.IsExistFeed(someCurrency, currency))
            {
                var feedName = _metadataRepo.GetFeedInfo(someCurrency, currency).Result.First().Name;
                step.FeedName = feedName;
                step.PathFound = true;
            }
            return step;
        }
        private async Task<decimal> ConvertIndirectAsync(decimal amount, List<ExchangeStep> steps, DateTime date)
        {
            var id = Guid.NewGuid().ToString("N");
            var firstStep = steps.First(x => x.Order == 1);
            _logger.LogDebug($"START indirect convert . id: {id} steps:{steps.Count()} from:{firstStep.SourceCurrency} amount now: {amount}");
            for (int stepNum = 0; stepNum < steps.Count; stepNum++)
            {
                var step = steps.Find(s => s.Order == stepNum + 1);
                amount = await Convert(amount, step.SourceCurrency, step.TargetCurrency, date, tryInDirect: false);
                _logger.LogDebug($"indirect convert. id: {id} step:{stepNum} from:{step.SourceCurrency} to:{step.TargetCurrency} amount now: {amount}");
            }
            return amount;
        }
        #endregion
    }
}
