﻿namespace UF.Core
{
    public class ProviderSymbol
    {
        public string ProviderName { get; set; }
        public string SymbolName{ get; set; }
    }
}