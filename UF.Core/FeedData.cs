﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UF.Core
{
    public class FeedData
    {
        public string Name { get; set; }
        public string BaseCurrency { get; set; }
        public string OtherCurrency { get; set; }
        public List<ProviderSymbol> Sources { get; set; } = new List<ProviderSymbol>();

        public FeedInfo ToFeedInfo()
        {
            return new FeedInfo()
            {
                Name = Name,
                BaseCurrency = this.BaseCurrency,
                OtherCurrency = this.OtherCurrency
            };
        }
    }

    public class FeedInfo
    {
        public string Name { get; set; }
        public string BaseCurrency { get; set; }
        public string OtherCurrency { get; set; }

    }
}
