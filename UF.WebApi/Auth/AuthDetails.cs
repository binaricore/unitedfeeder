﻿namespace UF.WebApi.Auth
{
    public class AuthDetails
    {
        public string Name { get; set; }
        public string UserRole { get; set; }
    }
}
