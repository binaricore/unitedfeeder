﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.Application.Queries;
using UF.WebApi.Auth;

namespace UF.WebApi.Controllers
{
    
    [ApiController]
    [Authorize(Roles = UsersRoles.MultipleRolesForAuthorize.ADMIN_OR_REGULAR)]
    public class CurrenciesController : ControllerBase
    {
        private ILogger<CurrenciesController> _logger;
        private ISender _mediator;

        public CurrenciesController(ILogger<CurrenciesController> logger,
                                    ISender mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        [Route("/api/currencies.json")]
        public async Task<IActionResult> GetCurrenciesAsync()
        {
            var symbols = await _mediator.Send(new GetCurrenciesRequest());

            var response = symbols.ToDictionary(s => s, s => s);
            return Ok(response);
        }

    }

}
