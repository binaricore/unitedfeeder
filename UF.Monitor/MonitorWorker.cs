using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reactive.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Interfaces;
using UF.Infra;

namespace UF.Monitor
{
    public class MonitorWorker : BackgroundService
    {
        private readonly ILogger<MonitorWorker> _logger;
        private readonly INotifier _notifier;
        private readonly int _monitorRateInSeconds;

        public MonitorWorker(ILogger<MonitorWorker> logger, INotifier notifier)
        {
            _logger = logger;
            _notifier = notifier;
            _monitorRateInSeconds = 
                int.TryParse(Environment.GetEnvironmentVariable("MONITOR_RATE_SECONDS"), out int rate) 
                ? rate 
                : 60;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Run(() =>
            {
                _logger.LogInformation($"Checking every {_monitorRateInSeconds} seconds.");
                CheckUnitedFeederServiceAvailabilty(stoppingToken);
            });
        }

        private void CheckUnitedFeederServiceAvailabilty(CancellationToken stoppingToken)
        {
            Observable.Interval(TimeSpan.FromSeconds(_monitorRateInSeconds)) 
                       .Subscribe(async _ =>
                       {
                           try
                           {
                               // 1. check api's health service
                               using var webClient = new WebClient();
                               var res = await webClient.DownloadStringTaskAsync
                               ($@"https://agg.binarytradingcore.com/health");
                               if (res != "Healthy")
                               {
                                   throw new Exception("Not Healthy");
                               }
                               
                               // 2. check api 'historical' service
                               var day = DateTime.UtcNow.Date.ToString("yyyy-MM-dd");
                               res = await
                                    webClient.DownloadStringTaskAsync
                                    ($"https://agg.binarytradingcore.com/api/historical/{day}.json?base=USD&symbols=BTC&app_id=cgN8mhM3Eu77c3R");
                               
                               // 3. check api 'latest' service
                               res = await webClient.DownloadStringTaskAsync
                               ($@"https://agg.binarytradingcore.com/api/latest.json?app_id=cgN8mhM3Eu77c3R&base=USD");

                               _logger.LogInformation($"United Feeder Monitor - Service Available");
                               
                           }
                           catch (Exception ex)
                           {
                               _logger.LogError(ex, $"United Feeder Monitor - Service Not Available.");
                               _notifier.Notify("United Feeder Monitor - Service Not Available.");
                           }
                       }, onError: e => {
                           _logger.LogError($"CheckUnitedFeederServiceAvailabilty error",e);
                           Environment.Exit(1); },
                       onCompleted: () => {
                           _logger.LogError($"CheckUnitedFeederServiceAvailabilty completed");
                           Environment.Exit(1); },
                       stoppingToken);
        }

        internal class OpenExchangeRates
        {
            public string disclaimer { get; set; }
            public string license { get; set; }
            public int timestamp { get; set; }
            public string @base { get; set; }
            public Dictionary<string, decimal> rates { get; set; }
        }
    }
}
