﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UF.Application.Commands;
using UF.Application.Queries;
using UF.Core;
using UF.WebApi.Auth;
using UF.WebApi.Auth.DB;

namespace UF.WebApi.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = UsersRoles.ADMIN_USER)]
    public class FeedConfigController : ControllerBase
    {
        private ISender _mediator;
        private ILogger<FeedConfigController> _logger;

        public FeedConfigController(ILogger<FeedConfigController> logger,
                                    ISender mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            
            var feedData = await _mediator.Send(new GetFeedDataRequest());
            return Ok(feedData);
        }

        [HttpGet]
        [Route("{name}")]
        public async Task<IActionResult> Get(string name)
        {
            var feedData = await _mediator.Send(new GetFeedDataByNameRequest(name));
            return Ok(feedData);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody]  List<FeedData> feedDataList)
        {
            var res = await _mediator.Send(new FeedDataAddRequest(feedDataList));
            return Ok(res);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] string name)
        {
            var result = await _mediator.Send(new FeedDataDeleteRequest(name));
            return Ok(result);
        }
    }
}
