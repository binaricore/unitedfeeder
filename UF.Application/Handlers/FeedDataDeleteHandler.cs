﻿using MediatR;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UF.Application.Commands;
using UF.Application.Helpers;
using UF.Application.Interfaces;
using UF.Application.Queries;
using UF.Core;

namespace UF.Application.Handlers
{
    public class FeedDataDeleteHandler : IRequestHandler<FeedDataDeleteRequest, List<FeedData>>
    {

        private readonly IFeedDataRepo _feedDataRepo;
        private readonly IDatabase _redis;
        private readonly ILogger<FeedDataDeleteHandler> _logger;
        private readonly ISerializer _serializer;

        public FeedDataDeleteHandler(IFeedDataRepo feedDataRepo, ISerializer serializer, ILogger<FeedDataDeleteHandler> logger)
        {
            _feedDataRepo = feedDataRepo;
            _serializer = serializer;
            _logger = logger;
        }

        public async Task<List<FeedData>> Handle(FeedDataDeleteRequest request, CancellationToken cancellationToken)
        {
            return await _feedDataRepo.DeleteFeedData(request.Name);
        }
    }
}
